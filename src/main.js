import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
// globally filter created
Vue.filter('snippet', val => {
  if(!val ||  typeof(val) != 'string') return ''
  // else na bolle o hobe
  val = val.slice(0, 30)
  return val
})

new Vue({
  render: h => h(App),
}).$mount('#app')
